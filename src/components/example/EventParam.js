import React from 'react'

export default class EventParam extends React.Component {
  constructor() {
    super()
    this.state = {
      number: [1, 2, 3, 4, 5, 6, 7, 8, 9],
      displayNumber: ''
    }
  }
  handleClick(value) {
    this.setState((state) => 
      ({ displayNumber: state.displayNumber += value })
    )
  }
  render() {
    return (
      <div className="w-1/2 mx-auto p-4">
        <h1 className="text-4xl font-bold text-center mb-4">{this.state.displayNumber}</h1>
        <div className="grid grid-cols-3 gap-2">
        {this.state.number.map((item, index) => {
          return (
            <button
              type="button"
              className="bg-blue-400 hover:bg-blue-500 text-white font-bold py-2 px-3 rounded"
              key={index}
              onClick={(e) => this.handleClick(item, e)}>
              {item}
            </button>
          )
        })}
        </div>
      </div>
    )
  }
}


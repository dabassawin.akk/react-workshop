import React from 'react'
import styled from 'styled-components'

const Container = styled.div`
  text-align: center;
  padding: 4px;
`
const Title = styled.h1`
  font-size: 24px;
  font-weight: bold;
`

export default class StyledComponent extends React.Component {
  render() {
    return (
      <Container>
        <Title>React Workshop</Title>
      </Container>
    )
  }
}

import React from 'react'
import classNames from 'classnames/bind'
import style from './style.module.scss'

let cx = classNames.bind(style)

export default class Style extends React.Component {
  render() {
    return (
      <div className={cx('container')}>
        <h1 className={cx({'title': true})}>React Workshop</h1>
      </div>
    )
  }
}

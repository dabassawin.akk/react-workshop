import React from 'react'

export default class Routing extends React.Component {
  goToHomePage() {
    this.props.history.push('/')
  }
  render() {
    return (
      <button
        onClick={() => this.goToHomePage()}>
        Go to home page
      </button>
    )
  }
}


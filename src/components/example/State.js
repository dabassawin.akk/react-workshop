import React from 'react'

export default class State extends React.Component {
  constructor() {
    super()
    this.state = {
      id: '',
      person: {
        firstName: '',
        lastName: ''
      },
      colors: []
    }
  }
  updateId(e) {
    this.setState({
      id: e.target.value
    })
  }
  updatePerson(e) {
    this.setState((prevState) => ({
      person: {
        ...prevState.person,
        [e.target.name]: e.target.value
      }
    }))
  }
  updateColor(color) {
    this.setState((prevState) => {
      let newColors = prevState.colors
      if (newColors.indexOf(color) === -1) {
        newColors.push(color)
      }
      return {
        colors: newColors
      }
    })
  }
  render() {
    return (
      <div className="p-4">
        <div className="w-1/3 mb-6">
          <label className="block tracking-wide text-gray-700 text-xs font-bold mb-2" htmlFor="firstName">
            Id
          </label>
          <input
            className="appearance-none block w-full text-gray-700 border border-gray-200 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white"
            id="id"
            name="id"
            type="text"
            autoComplete="off"
            placeholder="id"
            onInput={(e) => this.updateId(e)} />
        </div>
        <div className="w-1/3 mb-6">
          <label className="block tracking-wide text-gray-700 text-xs font-bold mb-2" htmlFor="firstName">
            First name
          </label>
          <input
            className="appearance-none block w-full text-gray-700 border border-gray-200 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white"
            id="firstName"
            name="firstName"
            type="text"
            autoComplete="off"
            placeholder="First name"
            onInput={(e) => this.updatePerson(e)} />
        </div>
        <div className="w-1/3 mb-6">
          <label className="block tracking-wide text-gray-700 text-xs font-bold mb-2" htmlFor="lastName">
            Last name
          </label>
          <input
            className="appearance-none block w-full text-gray-700 border border-gray-200 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white"
            id="lastName"
            name="lastName"
            type="text"
            autoComplete="off"
            placeholder="Last name"
            onInput={(e) => this.updatePerson(e)} />
        </div>
        <div className="flex mb-6">
          <button
            className="bg-red-500 hover:bg-red-600 text-white font-bold py-2 px-3 mr-2 rounded"
            onClick={() => this.updateColor('red')}>
            Red
          </button>
          <button
            className="bg-green-500 hover:bg-green-600 text-white font-bold py-2 px-3 mr-2 rounded"
            onClick={() => this.updateColor('green')}>
            Green
          </button>
          <button
            className="bg-blue-500 hover:bg-blue-600 text-white font-bold py-2 px-3 rounded"
            onClick={() => this.updateColor('blue')}>
            Blue
          </button>
        </div>
        <h1 className="text-2xl font-bold">{`Id: ${this.state.id}`}</h1>
        <h1 className="text-2xl font-bold">{`Full name: ${this.state.person.firstName} ${this.state.person.lastName}`}</h1>
        <h1 className="text-2xl font-bold">{`Colors: ${this.state.colors.join(', ')}`}</h1>
      </div>
    )
  }
}


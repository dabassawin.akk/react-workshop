import React from 'react'

export default class Private extends React.Component {
  render() {
    return (
      <div className="text-center text-2xl font-bold p-4">
        <h1>This is private component</h1>
      </div>
    )
  }
}

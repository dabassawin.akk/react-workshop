import React from 'react'

export default class Lifecycle extends React.Component {
  constructor() {
    super()
    this.state = {
      count: 0
    }
  }
  componentDidMount() {
    setInterval(() => {
      this.setState((state) => ({
        count: state.count + 1
      }))
    }, 2000)
    console.log('componentDidMount')
  }
  componentDidUpdate(prevProps, prevState) {
    console.log('componentDidUpdate', prevState.count)
  }
  componentWillUnmount() {
    console.log('componentWillUnmount')
  }
  render() {
    return <div>{this.state.count}</div>
  }
}

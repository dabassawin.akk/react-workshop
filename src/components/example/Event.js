import React from 'react'

export default class Event extends React.Component {
  constructor() {
    super()
    this.state = {
      number: 0
    }
  }
  handleClick() {
    this.setState((state) =>
      ({ number: state.number + 1 })
    )
  }
  render() {
    return (
      <div className="flex justify-center p-4">
        <div>
          <h1 className="text-4xl font-bold text-center mb-4">{this.state.number}</h1>
          <button
            type="button"
            className="bg-green-400 hover:bg-green-500 text-white font-bold py-2 px-3 rounded"
            onClick={() => this.handleClick()}>
            + Add
          </button>
        </div>
      </div>
    )
  }
}


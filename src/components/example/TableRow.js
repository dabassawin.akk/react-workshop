import React from 'react'

export default class TableRow extends React.Component {
  render() {
    return (
      <tr>
        <td className="border p-4 text-left">{this.props.user?.id}</td>
        <td className="border p-4 text-left">{this.props.user?.name}</td>
        <td className="border p-4 text-left">{this.props.user?.email}</td>
        <td className="border p-4 text-left">
          <button
            className="bg-red-500 hover:bg-red-600 text-white font-bold py-2 px-3 mr-2 rounded"
            onClick={() => this.props.removeUser(this.props.user?.id)}>
            Delete
          </button>
        </td>
      </tr>
    )
  }
}

import React from 'react'

export default class List extends React.Component {
  render() {
    const numbers = [1, 2, 3, 4, 5]
    const listItems = numbers.map((item, index) => {
      return <li key={index}>{item}</li>
    })
    return (
      <ul>
        {listItems}
      </ul>
    )
  }
}

import React from 'react'

export default class Condition extends React.Component {
  render() {
    const isLogin = false
    return (
      <div>
        Hello, <h1>{isLogin ? 'Member' : 'Stranger' }</h1>
      </div>
    )
  }
}

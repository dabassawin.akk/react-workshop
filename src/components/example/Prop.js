import React from 'react'
import TableRow from 'components/example/TableRow'

export default class Prop extends React.Component {
  constructor() {
    super()
    this.state = {
      users: [
        {
          id: 1,
          name: 'Leanne Graham',
          email: 'Sincere@april.biz'
        },
        {
          id: 2,
          name: 'Ervin Howell',
          email: 'Shanna@melissa.tv'
        },
        {
          id: 3,
          name: 'Clementine Bauch',
          email: 'Nathan@yesenia.net'
        }
      ]
    }
    this.removeUser = this.removeUser.bind(this)
  }
  removeUser(id) {
    this.setState((state) => {
      let updateUser = state.users.filter(user => user.id !== id)
      return {
        users: updateUser
      }
    })
  }
  render() {
    return (
      <div className="container mx-auto p-4">
        <table className="table-fixed w-full">
          <thead className="bg-gray-100">
            <tr>
              <th className="border p-4 text-left w-24">id</th>
              <th className="border p-4 text-left">name</th>
              <th className="border p-4 text-left">email</th>
              <th className="border p-4 text-left"></th>
            </tr>
          </thead>
          <tbody>
            {
              this.state.users.map((user) => (
                <TableRow
                  key={user.id}
                  user={user}
                  removeUser={this.removeUser}>
                </TableRow>
              ))
            }
          </tbody>
        </table>
      </div>
    )
  }
}

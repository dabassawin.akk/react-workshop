import React from 'react'
import { getUser } from 'services/api/users'

export default class HttpRequest extends React.Component {
  constructor() {
    super()
    this.state = {
      users: []
    }
  }
  componentDidMount() {
    getUser()
      .then(response => {
        this.setState({ users: response.data })
      })
      .catch(error => {
        console.log(error.message)
      })
  }
  render() {
    return <p>{JSON.stringify(this.state.users)}</p>
  }
}


import React from 'react'

export default class Home extends React.Component {
  constructor() {
    super()
    this.state = {
      projectName: 'React Workshop'
    }
  }
  render() {
    return (
      <div className="text-center text-2xl font-bold p-4">
        <h1>{this.state.projectName}</h1>
      </div>
    )
  }
}

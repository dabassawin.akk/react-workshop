import React from 'react'

export default class Form extends React.Component {
  constructor() {
    super()
    this.state = {
      firstName: '',
      lastName: ''
    }
  }
  handleInput(e) {
    this.setState({
      [e.target.name]: e.target.value
    })
  }
  render() {
    return (
      <div className="p-4">
        <div className="w-1/3 mb-6">
          <label className="block tracking-wide text-gray-700 text-xs font-bold mb-2" htmlFor="firstName">
            First name
          </label>
          <input
            className="appearance-none block w-full text-gray-700 border border-gray-200 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white"
            id="firstName"
            name="firstName"
            type="text"
            autoComplete="off"
            placeholder="First name"
            onInput={(e) => this.handleInput(e)} />
        </div>
        <div className="w-1/3 mb-6">
          <label className="block tracking-wide text-gray-700 text-xs font-bold mb-2" htmlFor="lastName">
            Last name
          </label>
          <input
            className="appearance-none block w-full text-gray-700 border border-gray-200 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white"
            id="lastName"
            name="lastName"
            type="text"
            autoComplete="off"
            placeholder="Last name"
            onInput={(e) => this.handleInput(e)} />
        </div>
        <h1 className="text-2xl font-bold">{`Full name: ${this.state.firstName} ${this.state.lastName}`}</h1>
      </div>
    )
  }
}


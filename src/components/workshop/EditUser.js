import React from 'react'

export default class EditUser extends React.Component {
  constructor(props) {
    super(props)
    this.state = {}
  }
  render() {
    return (
      <div className="container mx-auto p-4">
        <form>
          <div className="w-full mb-6 md:mb-0">
            <label className="block tracking-wide text-gray-700 text-xs font-bold mb-2" htmlFor="firstName">
              Name
            </label>
            <input
              className="appearance-none block w-full text-gray-700 border border-gray-200 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white"
              id="firstName"
              type="text"
              autoComplete="off"
              placeholder="Name" />
          </div>
          <div className="w-full mb-6 md:mb-0">
            <label className="block tracking-wide text-gray-700 text-xs font-bold mb-2" htmlFor="email">
              Email
            </label>
            <input
              className="appearance-none block w-full text-gray-700 border border-gray-200 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white"
              id="email"
              type="text"
              autoComplete="off"
              placeholder="Email" />
          </div>
          <div className="flex flex-wrap -mx-3 mb-2">
            <div className="w-full md:w-1/4 px-3 mb-6 md:mb-0">
              <label className="block tracking-wide text-gray-700 text-xs font-bold mb-2" htmlFor="street">
                Street
              </label>
              <input
                className="appearance-none block w-full text-gray-700 border border-gray-200 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white"
                id="street"
                type="text"
                autoComplete="off"
                placeholder="Street" />
            </div>
            <div className="w-full md:w-1/4 px-3 mb-6 md:mb-0">
              <label className="block tracking-wide text-gray-700 text-xs font-bold mb-2" htmlFor="suite">
                Suite
              </label>
              <input
                className="appearance-none block w-full text-gray-700 border border-gray-200 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white"
                id="suite"
                type="text"
                autoComplete="off"
                placeholder="Suite" />
            </div>
            <div className="w-full md:w-1/4 px-3 mb-6 md:mb-0">
              <label className="block tracking-wide text-gray-700 text-xs font-bold mb-2" htmlFor="city">
                City
              </label>
              <input
                className="appearance-none block w-full text-gray-700 border border-gray-200 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white"
                id="city"
                type="text"
                autoComplete="off"
                placeholder="City" />
            </div>
            <div className="w-full md:w-1/4 px-3 mb-6 md:mb-0">
              <label className="block tracking-wide text-gray-700 text-xs font-bold mb-2" htmlFor="zipcode">
                Zipcode
              </label>
              <input
                className="appearance-none block w-full text-gray-700 border border-gray-200 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white"
                id="zipcode"
                type="text"
                autoComplete="off"
                placeholder="Zipcode" />
            </div>
          </div>
          <div className="flex flex-wrap -mx-3 mb-2">
            <div className="w-full md:w-1/6 px-3 mb-6 md:mb-0">
              <button type="button" className="w-full bg-green-400 hover:bg-green-500 text-white font-bold py-2 px-3 rounded">
                Save
              </button>
            </div>
            <div className="w-full md:w-1/6 px-3 mb-6 md:mb-0">
              <button type="button" className="w-full bg-gray-400 hover:bg-gray-500 text-white font-bold py-2 px-3 rounded">
                Back
              </button>
            </div>
          </div>
        </form>
      </div>
    )
  }
}

import React from 'react'

export default class User extends React.Component {
  constructor(props) {
    super(props)
    this.state = {}
  }
  render() {
    return (
      <div className="container mx-auto p-4">
        <table className="table-fixed w-full">
          <thead className="bg-gray-100">
            <tr>
              <th className="border p-4 text-left w-24">id</th>
              <th className="border p-4 text-left">name</th>
              <th className="border p-4 text-left">email</th>
              <th className="border p-4 text-left">address</th>
            </tr>
          </thead>
          <tbody>
            <tr className="hover:bg-blue-50 cursor-pointer">
              <td className="border p-4 text-left">1</td>
              <td className="border p-4 text-left">Leanne Graham</td>
              <td className="border p-4 text-left">Sincere@april.biz</td>
              <td className="border p-4 text-left">Kulas Light Apt. 556 Gwenborough 92998-3874</td>
            </tr>
          </tbody>
        </table>
      </div>
    )
  }
}

import React from "react"
import { BrowserRouter as Router, Switch, Route } from "react-router-dom"
import Private from "components/example/Private"
import Home from "components/example/Home"
import Jsx from "components/example/Jsx"
import Condition from "components/example/Condition"
import List from "components/example/List"
import Event from "components/example/Event"
import EventParam from "components/example/EventParam"
import Form from "components/example/Form"
import Style from "components/example/Style"
import State from "components/example/State"
import Prop from "components/example/Prop"
import Lifecycle from "components/example/Lifecycle"
import HttpRequest from "components/example/HttpRequest"
import Routing from "components/example/Routing"
import User from "components/workshop/User"
import EditUser from "components/workshop/EditUser"

const PrivateRoute = ({component: Component, ...rest}) => {
  const isLogin = true
  return (
    <Route {...rest} render={props => (
      isLogin ? <Component {...props} /> : null
    )} />
  )
}

export default class App extends React.Component {
  render() {
    return (
      <Router>
        <Switch>
          <Route exact path="/" component={Home} />
          <Route exact path="/jsx" component={Jsx} />
          <Route exact path="/condition" component={Condition} />
          <Route exact path="/list" component={List} />
          <Route exact path="/event" component={Event} />
          <Route exact path="/event-param" component={EventParam} />
          <Route exact path="/form" component={Form} />
          <Route exact path="/Style" component={Style} />
          <Route exact path="/state" component={State} />
          <Route exact path="/prop" component={Prop} />
          <Route exact path="/lifecycle" component={Lifecycle} />
          <Route exact path="/http-request" component={HttpRequest} />
          <Route exact path="/routing" component={Routing} />
          <Route exact path="/workshop/user" component={User} />
          <Route exact path="/workshop/user/:id" component={EditUser} />
          <PrivateRoute exact path="/private" component={Private}></PrivateRoute>
        </Switch>
      </Router>
    )
  }
}

const Http = require("../http")

export function getUser() {
  return Http.default.get(`https://jsonplaceholder.typicode.com/users`)
}

export function getUserById(id) {
  return Http.default.get(`https://jsonplaceholder.typicode.com/users/${id}`)
}

export function createUserById() {
  return Http.default.post(`https://jsonplaceholder.typicode.com/users`)
}

export function updateUserById(id, body) {
  return Http.default.patch(`https://jsonplaceholder.typicode.com/users/${id}`, body)
}

export function deleteUserById(id) {
  return Http.default.delete(`https://jsonplaceholder.typicode.com/users/${id}`)
}

const Axios = require('axios')

const axios = Axios.create({
  headers: {
    'Content-Type': 'application/json; charset=utf-8'
  }
})

export default axios
